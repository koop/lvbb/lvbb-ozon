# LVBB - Ozon
Dit is de repository met artefacten die nodig zijn voor de informatieuitwisseling tussen LVBB en Ozon. In de main branch van de repository staan geen bestanden (op deze README na). De artefacten zijn te vinden in de branch die naar de  release is genoemd waar de artefacten voor bedoeld zijn. 

- Zie het [documentatie-overzicht](https://koop.gitlab.io/lvbb/lvbb-ozon/index.html) voor een overzicht van de beschikbare versies van de artefacten. 

- De schemata kunnen in deze repository worden teruggevonden onder de *tag* met dezelfde naam als het versienummer van de standaard. Deze kunnen worden verkregen door:
  - in de ["Web-interface van Gitlab"](https://gitlab.com/koop/lvbb/lvbb-ozon) eerst het versienummer te kiezen (in plaats van *main*) en daarna de repository te downloaden;
  - met een eigen Git-client deze repository te klonen, daarna de tag op te zoeken en op basis van de tag een [Git kloon](https://git-scm.com/docs/git-clone) te maken of een [Git switch](https://git-scm.com/docs/git-switch) uit te voeren.
